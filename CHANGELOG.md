## 1.1.1 - 2019-01-16
* import from Github
* upgrade to mariadb 10.2.19-r1
* upgrade to Alpine 3.8.2
* add gitlab-ci

## 1.0.1 - 2017-12-08
* upgrade to mariadb 10.1.28-r1
* Custom base image: Alpine Linux arm32v6 ver 3.7 with qemu-arm-static
* Tested on Raspberry Pi 1 Model B Rev 2 with  Raspbian 4.9.51-1+rpi3 (2017-10-22) armv6l GNU/Linux and Docker v17.10.0-ce (It looks Docker 17.11.0 has the problem. sudo apt install docker-ce=17.10.0~ce-0~raspbian)

## 1.0.0 - 2017-10-30
* first release
* Custom base image: Alpine Linux arm32v6 ver 3.6 with qemu-arm-static
* Tested on Raspberry Pi 1 Model B Rev 2 with Arch Linux 4.9.58-1 and Docker v17.10.0-ce
